# Google Maps API - Places Module

---

Drop this Javascript module class into any JS framework, and bam now you have a 
Google map that plots markers and clusters of markers from query text searches.

### Dependencies:
1. Google Maps API - Places: https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places

The advanced clustering library for the Google Maps JavaScript API v3.
Can be found here: https://github.com/googlemaps/js-marker-clusterer

## Methods:

### Event: PlacesResultsEvent: 
```
GooglePlaces.PlacesResultsEvent
```

### Initial setup:
```
var markerIcon = {
	url: 'assets/img/icons/discover-icon-anchor.png',
	scaledSize: new google.maps.Size(27,40),
	origin: new google.maps.Point(0,0),
	anchor: new google.maps.Point(0,0)
};

var markerClusterIcon = {
	url: 'assets/img/icons/discover-icon-blank-sm.png',
	width: 27,
	height: 41,
	anchor: [8,0],
	textColor: '#004f9e',
	textSize: 11,
	fontWeight: 'bold'
};

var placesObj = {
	target: defaultMapDiv, 
	query: defaultQuery, 
	radius: defaultRadius, 
	ignoreTypes: ['car_repair', 'car_dealer', 'insurance_agency'], 
	markerIcon: markerIcon, 
	markerClusterIcon: markerClusterIcon
};

GooglePlaces(placesObj);
```

### Method: Map Search: 
```
GooglePlaces.mapSearch('car dealers', '50');
```

### Method: Get Google Map Results:
```
GooglePlaces.getGoogleMapResults(); // Get map results.
```

### Method: Ping For Place Detail: 
```
GooglePlaces.pingForPlaceDetail(placeId); // Get the place detail object called.
```

### Method: Get The Current Place Detail Object:
```
GooglePlaces.getCurrentPlaceDetailObject();
```

