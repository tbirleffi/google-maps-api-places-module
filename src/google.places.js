/**
 * Name: Google Places API - Module
 * Author: Tony Birleffi
 * Version: Alpha 1
 *
 * A basic javascript class for searching the Google Maps API and Places,
 * from basic text search queries. You'll always get up to 20 places at a time.
 *
 * TODO: Get user's latlng from HTML5 browser supported environments.
 * TODO: Add in Air BnB Javascript standards into this file: https://github.com/airbnb/javascript
 *
 * The advanced clustering library for the Google Maps JavaScript API v3.
 * Can be found here: https://github.com/googlemaps/js-marker-clusterer
 *
 * 
 * Methods:
 * Listen for Event: GooglePlaces.PlacesResultsEvent, to get results.
 *			
 * var markerIcon = {
 *		url: 'assets/img/icons/discover-icon-anchor.png',
 *		scaledSize: new google.maps.Size(27,40),
 *		origin: new google.maps.Point(0,0),
 *		anchor: new google.maps.Point(0,0)
 *	};
 *
 * var markerClusterIcon = {
 *		url: 'assets/img/icons/discover-icon-blank-sm.png',
 *		width: 27,
 *		height: 41,
 *		anchor: [8,0],
 *		textColor: '#004f9e',
 *		textSize: 11,
 *		fontWeight: 'bold'
 *	};
 *
 * var placesObj = {
 *		target: defaultMapDiv, 
 *		query: defaultQuery, 
 *		radius: defaultRadius, 
 *		ignoreTypes: ['car_repair', 'car_dealer', 'insurance_agency'], 
 *		markerIcon: markerIcon, 
 *		markerClusterIcon: markerClusterIcon
 *	};
 *
 * Init method: GooglePlaces(placesObj);
 * Additional map searches: GooglePlaces.mapSearch('car dealers', '50');
 *
 * To get extra place detail info, 1st call: 
 * GooglePlaces.pingForPlaceDetail(placeId); // Get the place detail object called.
 *
 * Then to get the results:
 * GooglePlaces.getCurrentPlaceDetailObject();
 *
 * To get all the map results:
 * GooglePlaces.getGoogleMapResults(); // Get map results.
**/

!function(global, google) {
	'use strict';

	var previousGooglePlaces = global.GooglePlaces;

	function GooglePlaces(options) {
		GooglePlaces.target = options.target;
		GooglePlaces.query = options.query;
		GooglePlaces.radius = options.radius;
		GooglePlaces.total = 0;

		GooglePlaces.markers = [];
		GooglePlaces.infoWindows = [];
		GooglePlaces.placeDetails = [];
		GooglePlaces.results = [];

		GooglePlaces.map = {};
		GooglePlaces.service = {};
		GooglePlaces.markerCluster = null;
		GooglePlaces.currentPlaceObject = null;
		GooglePlaces.currentLocation = new google.maps.LatLng(44.9812659, -93.2716198);
		GooglePlaces.ignoreTypes = options.ignoreTypes;
		GooglePlaces.markerIcon = options.markerIcon;
		GooglePlaces.markerClusterIcon = options.markerClusterIcon;

		GooglePlaces.PlacesResultsEvent = 'PlacesResultsEvent';
		GooglePlaces.resultsEvent = new CustomEvent(GooglePlaces.PlacesResultsEvent);

		google.maps.event.addDomListenerOnce(window, 'load', initMap);
		google.maps.event.addDomListener(window, "resize", centerMap);
	}

	function initMap() {
		resetMap();
		GooglePlaces.map = new google.maps.Map(document.getElementById(GooglePlaces.target), {
			mapTypeControl: true,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.RIGHT_TOP
			},
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.DEFAULT,
				position: google.maps.ControlPosition.LEFT_TOP
			},
			center: GooglePlaces.currentLocation,
			zoom: 10,
			scrollwheel: false,
		});

		GooglePlaces.searchField = document.getElementById('pac-input');
		GooglePlaces.map.controls[google.maps.ControlPosition.TOP_LEFT].push(GooglePlaces.searchField);
		GooglePlaces.searchBox = new google.maps.places.SearchBox(GooglePlaces.searchField);

		GooglePlaces.service = new google.maps.places.PlacesService(GooglePlaces.map);
		google.maps.event.addListenerOnce(GooglePlaces.map, 'idle', performSearch);
		google.maps.event.addListener(GooglePlaces.map, 'dragend', changeMap);
		google.maps.event.addListener(GooglePlaces.map, 'drag', isMapDrag);
		google.maps.event.addListener(GooglePlaces.map, 'zoom_changed', changeMap);
		google.maps.event.addListener(GooglePlaces.searchBox, 'places_changed', searchBoxChanged);
	}

	function centerMap() {
		var center = GooglePlaces.map.getCenter();
		google.maps.event.trigger(GooglePlaces.map, "resize");
		GooglePlaces.map.setCenter(center); 
	}

	function initMap() {
		resetMap();
		GooglePlaces.map = new google.maps.Map(document.getElementById(GooglePlaces.target), {
			mapTypeControl: true,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.RIGHT_TOP
			},
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.DEFAULT,
				position: google.maps.ControlPosition.LEFT_TOP
			},
			center: GooglePlaces.currentLocation,
			zoom: 10,
			scrollwheel: false,
		});

		GooglePlaces.searchField = document.getElementById('pac-input');
		GooglePlaces.map.controls[google.maps.ControlPosition.TOP_LEFT].push(GooglePlaces.searchField);
		GooglePlaces.searchBox = new google.maps.places.SearchBox(GooglePlaces.searchField);

		GooglePlaces.service = new google.maps.places.PlacesService(GooglePlaces.map);
		google.maps.event.addListenerOnce(GooglePlaces.map, 'idle', performSearch);
		google.maps.event.addListener(GooglePlaces.map, 'dragend', changeMap);
		google.maps.event.addListener(GooglePlaces.map, 'drag', isMapDrag);
		google.maps.event.addListener(GooglePlaces.map, 'zoom_changed', changeMap);
		google.maps.event.addListener(GooglePlaces.searchBox, 'places_changed', searchBoxChanged);
	}

	function isMapDrag() {
		GooglePlaces.searchField.value = '';
	}

	function searchBoxChanged() {
		GooglePlaces.searchPlaces = GooglePlaces.searchBox.getPlaces();
		changeMap();
	}

	function changeMap() {
		resetMap();
		var lat = 0;
		var lng = 0;

		if(GooglePlaces.searchPlaces && GooglePlaces.searchPlaces.length == 1 
			&& GooglePlaces.searchField.value.length > 0) {
			lat = GooglePlaces.searchPlaces[0].geometry.location.k;
			lng = GooglePlaces.searchPlaces[0].geometry.location.D;
		} else {
			lat = GooglePlaces.map.getCenter().k;
			lng = GooglePlaces.map.getCenter().D;
		}

		GooglePlaces.currentLocation = new google.maps.LatLng(lat, lng);
		performSearch();
	}

	function performSearch() {
		var request = {
			location: GooglePlaces.currentLocation,
			radius: GooglePlaces.radius,
			query: GooglePlaces.query,
		};

		GooglePlaces.service = new google.maps.places.PlacesService(GooglePlaces.map);
		GooglePlaces.service.textSearch(request, parseMarkerResults);
	}

	function parseMarkerResults(results, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			var listResults = [];
			if (results.length == 0) {
				return;
			}

			GooglePlaces.total = results.length;
			for (var i = 0; i < GooglePlaces.total; i++) {
				var place = results[i];
				var types = place.types.toString();
				var ignore = false;

				for (var t = 0; t < GooglePlaces.ignoreTypes.length; t++) {
					var et = GooglePlaces.ignoreTypes[t];
					if(types.indexOf(et) == 0) ignore = true;
				}

				if(!ignore) {
					listResults.push(results[i]);
					var marker = new google.maps.Marker({
						map: GooglePlaces.map,
						position: place.geometry.location,
						title: place.place_id,
						icon: GooglePlaces.markerIcon,
					});

					google.maps.event.addListener(marker, 'mouseover', getPlaceDetailInfo);
					google.maps.event.addListener(marker, 'mouseout', getPlaceDetailInfo);
					google.maps.event.addListener(marker, 'click', function() {
						closeInfoWindows();
						var placeId = this.getTitle();
						var pd = getPlaceDetail(placeId);
						if(pd) {
							var infoWindow = new google.maps.InfoWindow();
							infoWindow.setContent(pd.content);
			        		infoWindow.open(GooglePlaces.map, this);
							GooglePlaces.infoWindows.push(infoWindow);
						}
					});

					GooglePlaces.markers.push(marker);
				}
			}

			if(GooglePlaces.searchPlaces && GooglePlaces.searchPlaces.length == 1 
				&& GooglePlaces.searchField.value.length > 0) {
				GooglePlaces.map.setCenter(GooglePlaces.currentLocation);
				GooglePlaces.searchField.value = '';
			}

			if(GooglePlaces.markerClusterIcon) {
				var clusterConfig = { 
					styles:[GooglePlaces.markerClusterIcon, GooglePlaces.markerClusterIcon, GooglePlaces.markerClusterIcon] 
				};
				GooglePlaces.markerCluster = new MarkerClusterer(GooglePlaces.map, GooglePlaces.markers, clusterConfig);
			}
			else GooglePlaces.markerCluster = new MarkerClusterer(GooglePlaces.map, GooglePlaces.markers);
			
			GooglePlaces.results = listResults;
			document.dispatchEvent(GooglePlaces.resultsEvent);
		}
	}

	function getPlaceDetailInfo() {
		var placeId = this.getTitle();
		GooglePlaces.service.getDetails({placeId:placeId}, function(place, status) {
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				var types = place.types.toString();
				types = types.replace('_', ' ');
				types = types.replace(',', ', ');

				var content = '<div id="content">' +
					'<h3>' + place.name + '</h3>' +
					'<ul>' +
						'<li>' + place.formatted_address + '</li>' +
						'<li>' + place.formatted_phone_number + '</li>' +
						'<li><a href="' + place.website + '">' + place.website + '</a></li>' +
						'<li>Types: ' + types + '</li>' +
					'</ul>' +
				'</div>';

				if( !ifPlaceDetailExist(place.place_id) ) GooglePlaces.placeDetails.push({placeId:place.place_id, content:content});
			}
		});
	}

	function ifPlaceDetailExist(placeId) {
		if(GooglePlaces.placeDetails && GooglePlaces.placeDetails.length > 0) {
			for (var i = 0; i < GooglePlaces.placeDetails.length; i++) {
				var id = GooglePlaces.placeDetails[i].placeId;
				if(id == placeId) {
					return true;
					break;
				}
			}
		}
		return false;
	}

	function getPlaceDetail(placeId) {
		if(GooglePlaces.placeDetails && GooglePlaces.placeDetails.length > 0) {
			for (var i = 0; i < GooglePlaces.placeDetails.length; i++) {
				var id = GooglePlaces.placeDetails[i].placeId;
				if(id == placeId) {
					return GooglePlaces.placeDetails[i];
					break;
				}
			}
		}
		return null;
	}

	function closeInfoWindows() {
		if(GooglePlaces.infoWindows && GooglePlaces.infoWindows.length > 0) {
			for (var i = 0; i < GooglePlaces.infoWindows.length; i++) {
				var iw = GooglePlaces.infoWindows[i];
				iw.close();
			}
		}
	}

	function clearClusters() {
		if(GooglePlaces.markerCluster) {
			GooglePlaces.markerCluster.clearMarkers();
			GooglePlaces.markerCluster = null;
		}
	}

	function resetMap() {
		clearClusters();
		GooglePlaces.markers = [];
		GooglePlaces.placeDetails = [];
		GooglePlaces.markerIndex = 0;
		GooglePlaces.total = 0;
		GooglePlaces.numReqs = 0;
	}

	GooglePlaces.noConflict = function noConflict() {
		global.GooglePlaces = previousGooglePlaces;
		return GooglePlaces;
	};

	GooglePlaces.getGoogleMapResults = function getGoogleMapResults() {
		return GooglePlaces.results;
	};

	GooglePlaces.pingForPlaceDetail = function(placeId) {
		GooglePlaces.service.getDetails({placeId:placeId}, function(place, status) {
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				GooglePlaces.currentPlaceObject = place;
			}
		});
	};

	GooglePlaces.getCurrentPlaceDetailObject = function() {
		return GooglePlaces.currentPlaceObject;
	};

	GooglePlaces.mapSearch = function(query, radius) {
		resetMap();
		GooglePlaces.query = query;
		GooglePlaces.radius = radius;
		performSearch();
	};

	global.GooglePlaces = GooglePlaces;

}(this, google);
