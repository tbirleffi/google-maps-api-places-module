var PlacesWidget = (function() 
{
	var self = {}, selector = {}, defaultRadius = '50', defaultMapDiv = 'map-canvas';

	function unbindEvents() {
		selector.filters.off('click', filterStatus);
		$(document).off(GooglePlaces.PlacesResultsEvent, getMapResults);
	}

	function bindEvents() {
		selector.filters.on('click', filterStatus);
		$(document).on(GooglePlaces.PlacesResultsEvent, getMapResults);
	}

	function getRowHtml(results, row) {
		var html = ((row == 0) ? '<div class="row first">' : '<div class="row">');
		for (var i = (row * 4); i < ((row * 4) + 4); i++) {
			var place = results[i];
			if(place) {
				var types = '';
				for (var j = 0; j < place.types.length; j++) {
					var eachType = place.types[j];
					types += '<li>' + eachType + '</li>';
				}

				html += '<div class="col-sm-3 col-md-3 col-lg-3"><div class="places-box">';
				html += '<h5>' + place.name + '</h5>';
				html += '<p>' + place.formatted_address + '</p>';
				html += '<strong>Types:</strong>';
				html += '<ul>' + types + '</ul>';
				html += '<a href="' + place.place_id + '" class="places-more-link">more</a>';
				html += '</div></div>';
			}
		}

		html += '</div>';
		return html;
	}

	function getMapResults() {
		var results = GooglePlaces.getGoogleMapResults();
		var total = results.length;
		var len = ( ( total == 1 ) ? 1 : Math.round(total/4) );
		var resultsHtml = '';
		selector.filterResultsTotalLabel.html(total + ' results');

		for (var i = 0; i < len; i++) resultsHtml += getRowHtml(results, i);
		selector.filterResults.html(resultsHtml);

		if(resultsHtml.length > 0) $(".places-more-link").on("click", placeMoreLinkClicked);
	}

	function placeMoreLinkClicked(e) {
		e.preventDefault();
		var placeId = $(this).attr('href');
		var parent = $(this).parent();
		GooglePlaces.pingForPlaceDetail(placeId);
		$(this).remove();

		setTimeout(function() {
			var place = GooglePlaces.getCurrentPlaceDetailObject();
			var html = '';
			if(place.formatted_phone_number != 'undefined') html += ('<div><a href="tel:' + place.formatted_phone_number + '">' + place.formatted_phone_number + '</a></div>');
			if(place.website != 'undefined') html += ('<div><a href="' + place.website + '" target="_blank">' + place.website + '</a></div>');
			parent.append(html);
		}, 1000);
	}

	function filterStatus(e) {
		e.preventDefault();
		selector.filters.each(function(){
			$(this).removeClass('selected');
		});

		var current = $(this);
		current.addClass('selected');
		var query = current.attr('query');
		current = current.attr('href');
		current = current.replace('#', '');
		GooglePlaces.mapSearch(query, defaultRadius);
	}

	return {
		initialize: function() {
			self = this;
			selector = {
				filters: $('#filter-nav a'),
				defaultFilter: $('#filter-nav a.selected'),
				filterResultsTotalLabel: $('#filter-results-total span'),
				filterResults: $('#filter-results'),
			}

			var defaultQuery = selector.defaultFilter.attr('query');

			var markerIcon = {
				url: 'assets/img/icons/discover-icon-anchor.png',
				scaledSize: new google.maps.Size(27,40),
				origin: new google.maps.Point(0,0),
				anchor: new google.maps.Point(0,0)
			};

			var markerClusterIcon = {
				url: 'assets/img/icons/discover-icon-blank-sm.png',
				width: 27,
				height: 49,
				anchor: [8,0],
				textColor: '#004f9e',
				textSize: 11,
				fontWeight: 'bold'
			};

			var placesObj = {
				target: defaultMapDiv, 
				query: defaultQuery, 
				radius: defaultRadius, 
				ignoreTypes: ['car_repair', 'car_dealer', 'insurance_agency'], 
				markerIcon: markerIcon, 
				markerClusterIcon: markerClusterIcon
			};

			GooglePlaces(placesObj);

			setTimeout(function()
			{
				unbindEvents();
				bindEvents();
				return self;
		    }, 200);
		}
	}
}());

$(document).ready(function() {	
   	PlacesApp = PlacesWidget.initialize();
});